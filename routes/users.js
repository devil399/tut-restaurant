var express = require('express');
var router = express.Router();
var userService = require('../services/user-service.js');

/* GET users listing. */
router.get('/', function(req, res, next) {
    res.send('respond with a resource');
});


/* GET /users/view */
router.get('/view', function(req, res, next) {
    res.send('user view');
});

/* GET /users/create */
router.get('/create', function(req, res, next) {
    res.render('users/create', {
        title: 'Create an account'
    });
});

/* handling form submissions */
router.post('/create', function(req, res, next) {
    userService.addUser(req.body, function(err) {
        if (err) {
            var vm = {
                title: 'Create an account',
                input: req.body,
                error: 'Something went wrong'
            }
            delete vm.input.password;
            return res.render('users/create', vm);
        }
        res.redirect('/orders');
    });
});

module.exports = router;
